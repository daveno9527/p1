class AgentsController < ApplicationController
#  before_filter :authenticate_user!
  def show
    @agents = Agent.all
  end

  def new
    @agent = Agent.new
  end
  def create
    @agent = Agent.new(agent_params)
    if @agent.save
      redirect_to agent_path
    else
      render new_agent_path
    end
  end


  def edit
    @agent = Agent.find(params[:format])
    @contacts = Contact.where(:agent_id => @agent.id)
  end
  def update
    @a = Agent.find(params[:format])
    @a.update_attributes(agent_params)

    redirect_to agent_path
  end

  def destroy
    @agent = Agent.find(params[:format])
    if @agent.destroy
      redirect_to agent_path
    else
      render agent_path
    end
  end

  def agent_params
    params.require(:agent).permit(:name)
  end
end
