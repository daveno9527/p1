require 'date'
require 'rexml/document'
require 'net/http'
include REXML
class FetchNewsController < ApplicationController
  def show
    arr = []
    uri = URI('https://www.discountlandlord.co.uk/news/feed/')
    xml_string = Net::HTTP.get(uri)
    if xml_string != nil
      xmldoc = Document.new(xml_string)
      xmldoc.elements.each("rss/channel/item"){
          |e|
        arr.push(e)
      }
      arr.sort_by{|x| Date.parse(x.elements["pubDate"].text)}.reverse.each{
          |e| puts "Title : " + e.elements["title"].text
        puts "Date : " + e.elements["pubDate"].text
      }
      @arr_news = arr.sort_by{|x| Date.parse(x.elements["pubDate"].text)}.reverse
    end
  end
end
