class ContactsController < ApplicationController
  def new
    @contact = Contact.new
    @contact.agent_id = params[:format].to_i
  end
  def create
    @c = Contact.new(contact_params)
    if @c.save
      redirect_to edit_agent_path(@c.agent_id)
    else
      redirect_to edit_agent_path(@c.agent_id)
    end
  end


  def edit
    @contact = Contact.find(params[:format])
  end
  def update
    @c = Contact.find(params[:format])
    @c.update_attributes(contact_params)
    redirect_to edit_agent_path(@c.agent_id)
  end

  def destroy
    @c = Contact.find(params[:format])
    if @c.destroy
      redirect_to edit_agent_path(@c.agent_id)
    else
      redirect_to edit_agent_path(@c.agent_id)
    end
  end



  def contact_params
    params.require(:contact).permit(:addr,:agent_id)
  end
end
